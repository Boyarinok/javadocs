import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {

        ArrayList<Character> list = new ArrayList<>();
        ArrayList<String> listSTR = new ArrayList<>();
        ArrayList<String> cobAV = new ArrayList<>();
        ArrayList<String> cobVE = new ArrayList<>();
        ArrayList<String> cobSI = new ArrayList<>();
        ArrayList<String> fincom = new ArrayList<>();

        File file = new File("C://Users//Nick//Desktop//cht//src//Main.java");

        try (Scanner sc = new Scanner(file)) {

            while (sc.hasNextLine()) {
                String line = sc.nextLine();

                char letters[] = line.toCharArray();

                if (letters.length != 0) {

                    for (int y = 0; y < letters.length; y++) {
                        if (letters[y] != ' ' && letters[y++] == '/' && letters[letters.length - 1] != '*') {
                            for (int x = y + 1; x < letters.length; x++) {
                                list.add(letters[x]);
                            }

                            StringBuilder builder = new StringBuilder(list.size());
                            for (Character ch : list) {
                                builder.append(ch);
                            }
                            builder.toString();

                            listSTR.add(builder.toString());
                            String res = listSTR.toString();
                            fincom.add(res.replaceAll(",", "").replace("[", "").replace("]", ""));

                            list.clear();
                            listSTR.clear();
                            break;
                        } else if (y < letters.length && letters[letters.length - 2] == '/' && letters[letters.length - 1] == '*') {
                            while (letters[letters.length - 2] != '*' && letters[letters.length - 1] != '/') {
                                if (letters[letters.length - 2] == '/' && letters[letters.length - 1] == '*') {
                                    letters = sc.nextLine().toCharArray();

                                } else {
                                    ArrayList<Character> letOTB = new ArrayList<>();

                                    for (int i = 0; i < letters.length; i++) {
                                        if (i != 0 && letters[i - 1] != ' ' && letters[i] == ' ') {
                                            letOTB.add(letters[i]);
                                        } else if (letters[i] == '@') {

                                            ArrayList<Character> otbcase = new ArrayList<>();

                                            for (int i1 = 0; i1 < letters.length; i1++) {
                                                if (letters[i1] != ' ') {
                                                    otbcase.add(letters[i1]);
                                                } else if (i1 != 0 && letters[i1] == ' ' && letters[i1 - 1] != ' ') {
                                                    otbcase.add(letters[i1]);
                                                }
                                            }

                                            StringBuilder builder = new StringBuilder(otbcase.size());
                                            for (Character ch : otbcase) {
                                                builder.append(ch);
                                            }
                                            builder.toString();

                                            listSTR.add(builder.toString());
                                            String res = listSTR.toString();
                                            String s = res.replaceAll(",", "").replace("[", "").replace("]", "");
                                            listSTR.clear();

                                            String sOTB[] = s.split(" ");

                                            int mun = letters[++i];
                                            switch (mun) {

                                                case 97:
                                                    for (int yAV = 1; yAV < sOTB.length; yAV++) {
                                                        cobAV.add(sOTB[yAV]);
                                                    }
                                                    letters = sc.nextLine().toCharArray();
                                                    i = 0;
                                                    break;
                                                case 118:
                                                    for (int yAV = 1; yAV < sOTB.length; yAV++) {
                                                        cobVE.add(sOTB[yAV]);
                                                    }
                                                    letters = sc.nextLine().toCharArray();
                                                    i = 0;
                                                    break;
                                                case 115:
                                                    for (int yAV = 1; yAV < sOTB.length; yAV++) {
                                                        cobSI.add(sOTB[yAV]);
                                                    }
                                                    letters = sc.nextLine().toCharArray();
                                                    i = 0;
                                                    break;
                                            }


                                        } else if (letters[i] != ' ' || letters[i] != '*' || letters[i] != '/') {

                                            letOTB.add(letters[i]);

                                        }
                                    }

                                    if (letOTB.size() != 0 && letters[letters.length - 2] != '*' && letters[letters.length - 1] != '/') {

                                        ArrayList<Character> end = new ArrayList<>();

                                        for (int i = 0; i < letters.length; i++) {
                                            if (letters[i] != ' ') {
                                                end.add(letters[i]);
                                            } else if (i != 0 && letters[i] == ' ' && letters[i - 1] != ' ') {
                                                end.add(letters[i]);
                                            }
                                        }

                                        StringBuilder builder = new StringBuilder(end.size());
                                        for (Character ch : end) {
                                            builder.append(ch);
                                        }
                                        builder.toString();

                                        listSTR.add(builder.toString());
                                        String res = listSTR.toString();
                                        fincom.add(res.replaceAll(",", "").replace("[", "").replace("]", ""));
                                        list.clear();
                                        listSTR.clear();

                                        letters = sc.nextLine().toCharArray();
                                    }
                                }
                            }
                        }
                    }
                }


            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        System.out.println(cobAV);
        System.out.println(cobVE);
        System.out.println(cobSI);
        System.out.println(fincom);


        String report = generateReport(cobAV,cobVE,cobSI,fincom);
        saveToFile(report, "C://Users//Nick//Desktop//reportTES.html");


    }

    private static String generateReport(ArrayList<String> cobAV,ArrayList<String> cobVE,ArrayList<String> cobSI,ArrayList<String> fincom) {
        StringBuilder stringBuilder = new StringBuilder()
                .append("<!DOCTYPE html>\n")
                .append("<html>\n")
                .append("<head>\n")
                .append("    <title>Javadoc comments</title>\n")
                .append("</head>\n")
                .append("<style>\n")
                .append("td{\n")
                .append("background: #B0C4DE;\n")
                .append("text-align: center;\n")
                .append("</style>\n")
                .append("<body>\n")

                .append("   <h1 style=\"font-size:25px;\">Javadoc comments</h1>\n")
                .append("<table bordercolor=#0000FF border=\"2\" width=\"100%\">\n")

                .append("<tr>\n")
                .append("<td width=\"37%\">Class</td>")
                .append("<td width=\"37%\">Description</td>")
                .append("<td width=\"10%\">Author</td>")
                .append("<td width=\"8%\">Version</td>")
                .append("<td width=\"8%\">Since</td>")
                .append("</tr>\n");

        for (int i = 0; i < 2; i++) {
            stringBuilder.append("<td>")
                    .append(fincom.get(i))
                    .append("</td>\n");
        }

        for (int i = 0; i < cobAV.size(); i++) {
            stringBuilder.append("<td>")
                    .append(cobAV.get(i))
                    .append("</td>\n");
        }

        for (int i = 0; i < cobVE.size(); i++) {
            stringBuilder.append("<td>")
                    .append(cobVE.get(i))
                    .append("</td>\n");
        }

        for (int i = 0; i < cobSI.size(); i++) {
            stringBuilder.append("<td>")
                    .append(cobSI.get(i))
                    .append("</td>\n");
        }

        stringBuilder
                .append("   </table>\n")

                .append("</body>\n")
                .append("</html>");

        return stringBuilder.toString();

    }

    private static void saveToFile(String report, String fileName) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(fileName));
        writer.write(report);
        writer.close();
    }

}
